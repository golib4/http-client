package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/schema"
	"gitlab.com/golib4/logger/logger"
	"io"
	"net/http"
	"net/url"
	"time"
)

type Client struct {
	host    string
	client  *http.Client
	encoder *schema.Encoder
	logger  logger.Logger
	params  Params
}

type Params struct {
	LogInfo          bool
	TimeoutInSeconds int
}

type HttpError struct {
	error
	Status string
}

type HeaderData struct {
	Name  string
	Value string
}

type RequestData struct {
	Path    string
	Headers []HeaderData
}

type JsonRequestData struct {
	Body any
	Data RequestData
}

type SchemaRequestData struct {
	Body any
	Data RequestData
}

type BytesRequestData struct {
	Body *bytes.Buffer
	Data RequestData
}

func NewClient(host string, logger logger.Logger, params Params) Client {
	if params.TimeoutInSeconds == 0 {
		params.TimeoutInSeconds = 30
	}
	return Client{
		host:    host,
		client:  &http.Client{Timeout: time.Duration(params.TimeoutInSeconds) * time.Second},
		encoder: schema.NewEncoder(),
		logger:  logger,
		params:  params,
	}
}

func (c Client) SendRawPostRequest(requestData BytesRequestData, responseDataContainer any) (*HttpError, error) {
	request, err := c.createRawPostRequest(requestData)
	if err != nil {
		return nil, fmt.Errorf("can not create post request: %s", err)
	}

	httpError, err := c.sendRequest(request, requestData.Data, responseDataContainer)
	if httpError != nil {
		return httpError, nil
	}
	if err != nil {
		return nil, fmt.Errorf("can not do post request: %s", err)
	}

	return nil, nil
}

func (c Client) SendJsonPostRequest(requestData JsonRequestData, responseDataContainer any) (*HttpError, error) {
	request, err := c.createPostRequest(requestData)
	if err != nil {
		return nil, fmt.Errorf("can not create post request: %s", err)
	}

	httpError, err := c.sendRequest(request, requestData.Data, responseDataContainer)
	if httpError != nil {
		return httpError, nil
	}
	if err != nil {
		return nil, fmt.Errorf("can not do post request: %s", err)
	}

	return nil, nil
}

func (c Client) SendGetRequest(requestData SchemaRequestData, responseDataContainer any) (*HttpError, error) {
	request, err := c.createGetRequest(requestData)
	if err != nil {
		return nil, fmt.Errorf("can not create get request: %s", err)
	}

	httpError, err := c.sendRequest(request, requestData.Data, responseDataContainer)
	if httpError != nil {
		return httpError, nil
	}
	if err != nil {
		return nil, fmt.Errorf("can not do get request: %s", err)
	}

	return nil, nil
}

func (c Client) sendRequest(request *http.Request, requestData RequestData, responseDataContainer any) (*HttpError, error) {
	request = c.addHeaders(request, requestData)

	authorization := request.Header.Get("Authorization")
	c.logInfo(fmt.Sprintf("auth header was: %s", authorization))

	response, err := c.client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("error while sending request: %s", err)
	}

	c.logInfo(fmt.Sprintf("http request was sent to host: %s", request.Host))

	httpError, err := c.deserializeResponse(response, responseDataContainer)
	if httpError != nil {
		return httpError, nil
	}
	if err != nil {
		return nil, fmt.Errorf("error while deserializing request: %s", err)
	}

	return nil, nil
}

func (c Client) createRawPostRequest(requestData BytesRequestData) (*http.Request, error) {
	jsonData, err := json.Marshal(requestData.Body)
	if err != nil {
		return nil, err
	}

	c.logInfo(fmt.Sprintf("post request was: %s \n", jsonData))

	request, err := http.NewRequest(http.MethodPost, c.buildUrl(requestData.Data), requestData.Body)
	if err != nil {
		return nil, err
	}

	return request, nil
}

func (c Client) createPostRequest(requestData JsonRequestData) (*http.Request, error) {
	jsonData, err := json.Marshal(requestData.Body)
	if err != nil {
		return nil, err
	}

	c.logInfo(fmt.Sprintf("post request was: %s \n", jsonData))

	request, err := http.NewRequest(http.MethodPost, c.buildUrl(requestData.Data), bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	return request, nil
}

func (c Client) createGetRequest(requestData SchemaRequestData) (*http.Request, error) {
	query, err := c.createEncodedQuery(requestData)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest(http.MethodGet, c.buildUrl(requestData.Data), nil)
	if err != nil {
		return nil, err
	}

	if query != nil {
		c.logInfo(fmt.Sprintf("get query was: %s \n", *query))
		request.URL.RawQuery = *query
	}
	if err != nil {
		return nil, err
	}

	return request, nil
}

func (c Client) buildUrl(requestData RequestData) string {
	return fmt.Sprintf("%s%s", c.host, requestData.Path)
}

func (c Client) addHeaders(request *http.Request, requestData RequestData) *http.Request {
	request.Header.Add("Content-Type", "application/json")

	if requestData.Headers == nil {
		return request
	}

	for _, header := range requestData.Headers {
		request.Header.Add(header.Name, header.Value)
	}

	return request
}

func (c Client) createEncodedQuery(requestData SchemaRequestData) (*string, error) {
	params := url.Values{}

	if requestData.Body == nil {
		return nil, nil
	}
	err := c.encoder.Encode(requestData.Body, params)
	if err != nil {
		return nil, err
	}

	query := params.Encode()

	return &query, nil
}

func (c Client) deserializeResponse(response *http.Response, responseDataContainer any) (*HttpError, error) {
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	c.logInfo(fmt.Sprintf("raw response was: %s", body))

	isSuccess := response.StatusCode >= 200 && response.StatusCode < 300
	if !isSuccess {
		return &HttpError{
			error:  fmt.Errorf("response status is %d. body: %s \n", response.StatusCode, body),
			Status: response.Status,
		}, nil
	}

	err = json.Unmarshal(body, responseDataContainer)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (c Client) logInfo(message string) {
	if !c.params.LogInfo {
		return
	}

	c.logger.Info(message)
}
